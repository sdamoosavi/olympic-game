"""this code create a generator class 
that would make any dataset into generator"""
try:
    import json
    import matplotlib.pyplot as plt
    from mpl_toolkits.basemap import Basemap
    import geopy

except:
    print("First, Download modules!")


class Golympic:

    @staticmethod
    def coordinates(path='coords.json'):
        return json.load(open(path,'r'))
    
    @staticmethod
    def gen_data(file='games.txt'):
        return (game.split() for game in open(file,'r'))

    @staticmethod
    def fast_glob_map(data='games.txt',coord='coords.json'):

        plt.figure(figsize=(15,15))
        world = Basemap(projection='ortho',lat_0=75,lon_0=0,resolution='c')
        
        world.drawcoastlines(linewidth=0.25)
        world.fillcontinents(color='#191919',lake_color='#000000') # dark grey land, black lakes
        world.drawmapboundary(fill_color='#000000')                # black background
        world.drawcountries(linewidth=0.1, color="w")              # thin white line for country borders
        
        coordinates = Golympic.coordinates(coord)
        for game in Golympic.gen_data(data):
            city = ' '.join(game[:-1])
            year = game[-1].strip('()')
            x,y = world(*coordinates[city])
            
            world.plot(x,y,'r.')
            plt.text(x,y,year,fontsize=8,ha='center',va='bottom',color='navy')
        plt.show()