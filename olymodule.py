"""This code read data from directory and clean it then plot it"""

try:
    import math, json, collections
    import numpy as np
    import matplotlib.pyplot as plt
#     import plotly.graph_objects as go
    from mpl_toolkits.basemap import Basemap
    import geopy
except:
    print('packages are not available!!')

class Olympic:
        
    @staticmethod
    def _opendata(filename='games.txt'):
        cities, years = [], []
        locations = {}
        geolocator = geopy.geocoders.Nominatim()
        for game in open(filename, 'r'):
            words = game.split()
            
            city = ' '.join(words[:-1])
            year= words[-1].strip(' () ')
            locations[city] = geolocator.geocode(city.split('/')[0])
            
            cities.append(city)
            years.append(year)
        return cities, years, locations
    
    def __init__(self, filename='games.txt', *args, **kwargs):
        self.cities = Olympic._opendata(filename)[0]
        self.years = Olympic._opendata(filename)[1]
        self.locations = Olympic._opendata(filename)[2]
        
    @staticmethod
    def showmap(filename='games.txt'):
        plt.figure(figsize=(15,15))
        m1 = Basemap(projection='merc',
                     llcrnrlat=-60,
                     urcrnrlat=65,
                     llcrnrlon=-180,
                     urcrnrlon=180,
                     lat_ts=0,
                     resolution='c')
        m1.fillcontinents(color='#191919',lake_color='#000000') # dark grey land, black lakes
        m1.drawmapboundary(fill_color='#000000')                # black background
        m1.drawcountries(linewidth=0.1, color="w")              # thin white line for country borders
                
        locations = Olympic._opendata(filename)[2]
        for city,pos in locations.items():
            m1.plot(pos.longitude,pos.latitude,'r.',markersize=10,latlon=True)
        
        plt.show()